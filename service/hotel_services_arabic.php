<?php

	require("../config/config.inc.php"); 
	require("../config/Database.class.php");
	require("../config/Application.class.php");
	
	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();

	$jsonArray 		= 	array();
	$filling_categorys	=	array();
	$additionalFillings	=	array();
	$mealMenu		=	array();
	
	/**********************************************Filling category******************************************/
	
	$fillCatQuery	=	mysql_query("SELECT * FROM ".TABLE_FILLING_CATEGORY." order by id asc ");
    while($fillCatRow	=	mysql_fetch_array($fillCatQuery))
	{
		$fillings	=	array();
		$fillQuery	=	mysql_query("SELECT * FROM ".TABLE_FILLINGS." 
									  where filling_category_id=".$fillCatRow['id']." order by id asc ");
		if(mysql_num_rows($fillQuery)>0)
		{
			while($fillRow	=	mysql_fetch_array($fillQuery))
			{
				$filling_types	=	array();
				$fillTypeQuery	=	mysql_query("SELECT * FROM ".TABLE_FILLING_TYPE."
												  WHERE filling_id=".$fillRow['id']." order by id asc ");
				if(mysql_num_rows($fillTypeQuery)>0)
				{
					while($fillTypeRow	=	mysql_fetch_array($fillTypeQuery))
					{
						$data2['typeId']	=	(int)$fillTypeRow['id'];
						$data2['name']		=	$fillTypeRow['type_name'];
						$data2['titleArabic']	=	$fillTypeRow['type_name_arabic'];

						array_push($filling_types,$data2);
					}
				}
				$data1['fillingTypes'] 	= 	$filling_types;
				$data1['fillingId']	=	(int)$fillRow['id'];
				$data1['name']		=	$fillRow['filling_name'];
				$data1['titleArabic']	=	$fillRow['filling_name_arabic'];
				$data1['currency']	=	$fillRow['currency'];
				$data1['price']		=	$fillRow['price'];

				array_push($fillings,$data1);
			}
		}
		$filling_categorys['fillingCategories'] = 	$fillings;
		$filling_categorys['categoryId']	=	(int)$fillCatRow['id'];
		$filling_categorys['name']		=	$fillCatRow['category'];
		$filling_categorys['titleArabic']	=	$fillCatRow['category_arabic'];
		
		array_push($additionalFillings,$filling_categorys);
	}

	/**********************************************Meal type******************************************/
	
	$mealTypeQuery	=	mysql_query("SELECT * FROM ".TABLE_MEAL_TYPES." order by meal_order asc  ");
	while($mealTypeRow	=	mysql_fetch_array($mealTypeQuery))
	{
		$meal_types			=	array();
		if(strtolower($mealTypeRow['meal_type'])==strtolower('Additional fillings'))
		{
			$meal_types['isAddionalFilling']= 	true;
			$meal_types['mealId']		=	(int)$mealTypeRow['id'];
			$meal_types['name']		=	$mealTypeRow['meal_type'];
			$meal_types['titleArabic']	=	$mealTypeRow['meal_type_arabic'];
			array_push($mealMenu,$meal_types);
		}
		else
		{
			$menu_items			=	array();
			$menuItemQuery	=	mysql_query("SELECT * FROM ".TABLE_MENU_ITEMS." 
										  WHERE meal_type_id='".$mealTypeRow['id']."'  order by id asc ");
			if(mysql_num_rows($menuItemQuery)>0)
			{
				while($menuItemRow	=	mysql_fetch_array($menuItemQuery))
				{
					$data4['mealItemId']		=	(int)$menuItemRow['meal_type_id'];
					$data4['name']			=	$menuItemRow['menu_item'];
					$data4['titleArabic']		=	$menuItemRow['menu_item_arabic'];
					$data4['description']		=	$menuItemRow['description'];
					$data4['descriptionArabic']	=	$menuItemRow['description_arabic'];
					if($menuItemRow['isCalories']==1)
					{
						$data4['isCalories']	=	FALSE;
					}
					else
					{
						$data4['isCalories']	=	TRUE;
					}
					$data4['calories']		=	$menuItemRow['calories'];
					$data4['fat']			=	$menuItemRow['fat'];
					$data4['image']			=	$menuItemRow['image'];
					$data4['currency']		=	$menuItemRow['currency'];
					$data4['price']			=	$menuItemRow['price'];
                   			$data4['fillingAvailable']	=	$menuItemRow['filling_available'];
                   			$filling_items			=	array();
					if($menuItemRow['filling_available']==1)
					{
						$data4['fillingAvailable']	=	TRUE;
						$data4['fillingId']		=	(int)$menuItemRow['filling_id'];
						
						
						$data44=null;
						$menuFillingQuery	=	mysql_query("SELECT * FROM Item_filling WHERE itemID='".$menuItemRow['id']."' ");
						if(mysql_num_rows($menuFillingQuery	)>0)
						{
							while($menuFillingRow	=	mysql_fetch_array($menuFillingQuery))
							{
								$data44['filling']	=	$menuFillingRow	['item'];
								$data44['fillingArabic']=	$menuFillingRow	['itemArabic'];
								$data44['price']	=	$menuFillingRow	['price'];
								
								array_push($filling_items,$data44);
							}	
							
						}
						
						
						
					}
					else
					{
						$data4['fillingAvailable']	=	FALSE;
						unset($data4['fillingId']);
					}
					$data4['fillingsItem']	=$filling_items;
					array_push($menu_items,$data4);
				}
			}
			$meal_types['mealItems'] 		= 	$menu_items;
			$meal_types['isAddionalFilling']	= 	false;
			$meal_types['mealId']			=	(int)$mealTypeRow['id'];
			$meal_types['name']			=	$mealTypeRow['meal_type'];
			$meal_types['titleArabic']		=	$mealTypeRow['meal_type_arabic'];
			
			array_push($mealMenu,$meal_types);
		}		
	}
	/********************************************Meal type Ends***************************************/
	$jsonArray['additionalFillings']	=	$additionalFillings;
	$jsonArray['mealMenu']			=	$mealMenu;
		
	echo json_encode($jsonArray);
?>