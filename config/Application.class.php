<?php
session_start();
date_default_timezone_set('Asia/Kolkata'); 
class Application {


//Function : To encrypt and decrept string
//-
FUNCTION ENCRYPT_DECRYPT($Str_Message) {
//Function : encrypt/decrypt a string message v.1.0  without a known key
//Author   : Aitor Solozabal Merino (spain)
//Email    : aitor-3@euskalnet.net
//Date     : 01-04-2005
    $Len_Str_Message=STRLEN($Str_Message);
    $Str_Encrypted_Message="";
    FOR ($Position = 0;$Position<$Len_Str_Message;$Position++){
        // long code of the function to explain the algoritm
        //this function can be tailored by the programmer modifyng the formula
        //to calculate the key to use for every character in the string.
        $Key_To_Use = (($Len_Str_Message+$Position)+1); // (+5 or *3 or ^2)
        //after that we need a module division because can´t be greater than 255
        $Key_To_Use = (255+$Key_To_Use) % 255;
        $Byte_To_Be_Encrypted = SUBSTR($Str_Message, $Position, 1);
        $Ascii_Num_Byte_To_Encrypt = ORD($Byte_To_Be_Encrypted);
        $Xored_Byte = $Ascii_Num_Byte_To_Encrypt ^ $Key_To_Use;  //xor operation
        $Encrypted_Byte = CHR($Xored_Byte);
        $Str_Encrypted_Message .= $Encrypted_Byte;
       
        //short code of  the function once explained
        //$str_encrypted_message .= chr((ord(substr($str_message, $position, 1))) ^ ((255+(($len_str_message+$position)+1)) % 255));
    }
    RETURN $Str_Encrypted_Message;
} //end function


//Function : To stripslashes, mysql_real_escape_string, trim
//-
function convert($str)
 {
   $str1=stripslashes($str);
   $str2=trim($str1);
   return $str2;
 }
 
 
//Function : To split large sentence 
//-
function split_sentence($str,$count)
{
  if(strlen($str)>$count)
  {
   $str_content = substr($str, 0, $count);
   $str_pos = strrpos($str_content, " ");
   if ($str_pos>0)
    {
      $str_content = substr($str_content, 0, $str_pos);
      return $str_content."..."; 
	} 
   }
   else
   {
    return $str;
   }	
}
 
 
function showDate($dateTime, $format = "D, d/m/Y h:i A")
{
	$dateTime	=	strtotime($dateTime);
	return date($format,$dateTime);
}

function dbformat_date($exp_date) 
{
//echo $exp_date;die;
  $exp_date1=explode("/",$exp_date);
  $exp_date_original=$exp_date1[2]."-".$exp_date1[1]."-".$exp_date1[0];
  return $exp_date_original;
} 
function dbformat_date_db($exp_date) 
{
  $exp_date1=explode("-",$exp_date);
  $exp_date_original=$exp_date1[2]."/".$exp_date1[1]."/".$exp_date1[0];
  return $exp_date_original;
} 

function dbformat_time($exp_time) 
{
  $time =  date("H:i:s", strtotime($exp_time));
  return $time;
} 


function dbformat_date_db_with_hyphen($exp_date)
{
    $exp_date1=explode("-",$exp_date);
    $exp_date_original=$exp_date1[2]."-".$exp_date1[1]."-".$exp_date1[0];
    return $exp_date_original;
}



// Image validation
function validateImages($xtn) {
    $xtnArray = array("jpg", "png", "JPG", "jpeg", "JPEG");
    if (in_array($xtn, $xtnArray)) {
        return true;
    } else {
        return false;
    }
}


//Function : To crope image thumb
//-

function cropImage($w,$h,$filename,$stype,$dest)
{

$filename = $filename;

$width = $w;
$height = $h;
switch($stype) {
        case 'gif':
        $simg = imagecreatefromgif($filename);
        break;
        case 'jpg':
        $simg = imagecreatefromjpeg($filename);
        break;
        case 'png':
        $simg = imagecreatefrompng($filename);
        break;
		
    }

header('Content-type: image/jpeg');

list($width_orig, $height_orig) = getimagesize($filename);

$ratio_orig = $width_orig/$height_orig;

if ($width/$height > $ratio_orig) 
  {
   $width = $height*$ratio_orig;
  } 
  else 
 {
   $height = $width/$ratio_orig;
}


$image_p = imagecreatetruecolor($width, $height);

imagecopyresampled($image_p, $simg, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

imagejpeg($image_p,$dest, 100);
}

/**
* function created for ente angadi project
* $obj will be the json to be parsed
* will return the location ids as a string
*/
function parseLocationJson($obj)
{
	$location			=	array();
	foreach($obj as $key)
	{
		array_push($location,$key->locationId);
	}

	// location sent throught json for getting the shops
	$location		=	implode(",",$location); 
	return $location;
}
// for ente angadi
	
/**
* function for  validating whether the updating files are image or not 
* @pass the image name
* @return true if image format false if other formats
*/

	function imageValidation($path)
	{
		$path_info 	=	pathinfo($path);
		$ext	 	=	$path_info["extension"];
		$allowed 	=  	array('gif','png','jpg');		// allowed image extensions		
		if(in_array($ext,$allowed)) 	
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
/**
* function for  checking one value in multidimention array 
* @pass value to search and the array
* @return true if value is in the inside array
*/
	function in_multi_array($key, $array, $strict = false) 
	{
		$flag	=	"false";
	    foreach ($array as $item) 
	    {
	    	if(in_array($key,$item))
	    	{
				 $flag	=	"true";
			}
	    }
	    if($flag == "true")
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}	   
	}

/**
* function for  checking one value with key (shop_id=1) in multidimention array 
* @pass value to search and the array and arrayindax
* @return true if value is in the inside array
*/	
	function keyValueMultiArray($array, $key, $value)
	{
		$flag	=	"false";
	    if(is_array($array))
	    {
			foreach ($array as $subarray) 
		    {
		    	if(isset($subarray[$key]) && $subarray[$key] == $value) 
			    {
			        $flag	=	"true";
			    }
			    if(is_array($subarray))
		        {
					self::keyValueMultiArray($subarray, $key, $value);
				}
		    }
		}
		
	    if($flag == "true")
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
//removing html contents from inserting data
	
	function validateData($data)
	{
		$data	=	trim($data);
		$data	=	stripslashes($data);
		$data	=	htmlspecialchars($data);		
		$data	=	mysql_real_escape_string($data);

		return $data;
	}
	
	// function for creating session message
function sessionMsgCreate($type, $msg)
{
    if($type == "success"){
        return '<div id="session_modal" class="modal fade session_modal_success session_modal" role="dialog"><div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Successful</h4></div><div class="modal-body"><p>'.$msg.'</p></div><div class="modal-footer"><button type="button" class="btn btn-success" data-dismiss="modal">Close</button></div></div></div></div>';
    }
    elseif ($type == "error")
    {
        return '<div id="session_modal" class="modal fade session_modal_error session_modal" role="dialog"><div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Error</h4></div><div class="modal-body"><p>'.$msg.'</p></div><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal">Close</button></div></div></div></div>';
    }
}

function capitalize($str)
{
	$str	=	stripslashes($str);
	$str	=	trim($str);
	$str	=	strtolower($str);
	$str	=	ucwords($str);
	return $str;
}
};


$App = new Application();
//$App->path 	=	'http://localhost/dental';
//$App->path 	=	'http://moopans.dentricz.com



?>