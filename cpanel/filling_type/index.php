<?php
require('../admin_header.php');

if($_SESSION['hotelId']=="")
{
	header("location:../../logout.php");
}

$logID = $_SESSION['hotelId'];

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
   
//delete row in index page

function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>
    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Filling Type</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post">
                        	<select name="searchFill">
                        		<option value="">Select</option>
                               	<?php 
                               		$fillingQry	= mysql_query("SELECT id,filling_name,filling_name_arabic FROM ".TABLE_FILLINGS." ORDER BY filling_name ASC ");
                               		while($fillingRow = mysql_fetch_array($fillingQry))
                               		{
									?>
									<option value="<?php echo $fillingRow['id'];?>" <?php if($fillingRow['id']==@$_REQUEST['searchFill']){echo "selected";}?>><?php echo $fillingRow['filling_name']."(".$fillingRow['filling_name_arabic'].")";?></option>
									<?php
									}
                               		
                               	?>
                        	</select>
                            <input type="text" name="searchName" id="" placeholder="Category Name" value="<?php echo @$_REQUEST['searchName']; ?>">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                        </form>
                    </div>
                    <div class="page_nav_menu">
                        <ul class="page_nav">
                            <li>
                                <a href="add.php">
                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
<?php	
$cond="1";
if(@$_REQUEST['searchName'])
{
	$cond=$cond." and T.type_name like'%".$_REQUEST['searchName']."%'";
}
if(@$_REQUEST['searchFill'])
{
	$cond = $cond." and T.filling_id=".$_REQUEST['searchFill']."";
}

?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Filling</th>
                            <th>Type name in English</th>
                            <th>Type name in Arabic</th>               
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=0;
                    $selectAll = "SELECT T.id,T.type_name,T.type_name_arabic,F.filling_name
                    			    FROM ".TABLE_FILLING_TYPE." T 
                    		   LEFT JOIN ".TABLE_FILLINGS." F ON F.id=T.filling_id
                    			   WHERE $cond ORDER BY id desc";

                    $result = $db->query($selectAll);
                    if(mysql_num_rows($result)==0)
                    {
					?>
						<tr><td colspan="5" align="center">There is no data in list. </td></tr
					<?php
					}
					else
					{
	                    while ($row = mysql_fetch_array($result)) 
	                    {	
	                     ?>
	                     <tr>                            
	                        <td><?php echo ++$i; ?></td>
                            <td><?php echo $row['filling_name']; ?></td>
                            <td><?php echo $row['type_name']; ?></td>
	                        <td><?php echo $row['type_name_arabic']; ?></td>
	                        <td>
	                        <a class="show_table_lnk show_table_lnk_edit" href="edit.php?id=<?php echo $row['id'];?>">Edit</a>
	                        <a class="show_table_lnk show_table_lnk_del" onclick="return delete_type();" href="do.php?id=<?php echo $row['id'];?>&op=delete">Delete</a>
	                        </td>
	                    </tr>
	                        <?php
	                    }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>