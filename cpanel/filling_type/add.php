<?php
require('../admin_header.php');

if($_SESSION['hotelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form" enctype="multipart/form-data">
                <div class="bd_panel_head">
                    <h3>FILLING TYPE</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 form_block_row">
                        	<div class="form_block">
                                <label>Fillings<span class="valid">*</span> </label>
                               <select name="fillingId" required="">
                               	<option value="">Select</option>
                               	<?php 
                               		$fillingQry	= mysql_query("SELECT id,filling_name,filling_name_arabic FROM ".TABLE_FILLINGS." ORDER BY filling_name ASC ");
                               		while($fillingRow = mysql_fetch_array($fillingQry))
                               		{
									?>
									<option value="<?php echo $fillingRow['id'];?>"><?php echo $fillingRow['filling_name']."(".$fillingRow['filling_name_arabic'].")";?></option>
									<?php
									}
                               		
                               	?>
                               </select>
                            </div>
                             <div class="form_block">
                                <label>Type Name English <span class="valid">*</span></label>
                                <input type="text" name="typeEng" id="typeEng" required="">
                            </div>
                            <div class="form_block">
                                <label>Type Name Arabic </label>
                                <input type="text" name="typeArb" id="typeArb">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
