<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['hotelId']=="")
{
	header("location:../../logout.php");
}

$loginId = $_SESSION['hotelId'];

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':

            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success 	= 0;
			$date		= date("Y-m-d H:i:s");
            $data['category']			=	$App->capitalize($_REQUEST['categoryEng']);
            $data['category_arabic']	=	$App->convert($_REQUEST['categoryArb']);
           	$data['created_at']			=	$date;
           	
			$success = $db->query_insert(TABLE_FILLING_CATEGORY, $data);

            $db->close();
            
			if ($success) 
            {
                //$_SESSION['msg'] = " Details Added Successfully";
                $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
            } 
            header("location:index.php");
        break;
        
     case 'edit':
    		$editId = $_REQUEST['editId'];
    	
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
           	
           	$success 	= 0;
			$date		= date("Y-m-d H:i:s");
            $data['category']			=	$App->capitalize($_REQUEST['categoryEng']);
            $data['category_arabic']	=	$App->convert($_REQUEST['categoryArb']);
           	$data['updated_at']			=	$date;

			$success = $db->query_update(TABLE_FILLING_CATEGORY, $data, "id = $editId");

            $db->close();
			if ($success) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details uploaded successfully");                
            } 
            header("location:index.php");
        break;
    
    // DELETE SECTION
    case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success = 0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();

		$success1= @mysql_query("DELETE FROM `".TABLE_FILLING_CATEGORY."` WHERE id='{$deleteId}'");
		
        $db->close();
        if ($success1) 
        {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } else 
        {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
        }
        header("location:index.php");
        break;
}
?>