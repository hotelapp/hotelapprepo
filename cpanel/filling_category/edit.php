<?php
require('../admin_header.php');

if($_SESSION['hotelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

$editId		=	$_REQUEST['id'];
$editSelect = 	"select  * from ". TABLE_FILLING_CATEGORY." where id='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=edit" class="default_form" enctype="multipart/form-data">
            <input type="hidden" name="editId" value="<?php echo $editId; ?>">
                <div class="bd_panel_head">
                    <h3>FILLING CATEGORY</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 form_block_row">
                        	<div class="form_block">
                                <label>Category English <span class="valid">*</span> </label>
                                <input type="text" name="categoryEng" id="categoryEng" required="" value="<?php echo $editRow['category'];?>">
                            </div>
                            <div class="form_block">
                                <label>Category Arabic</label>
                                <input type="text" name="categoryArb" id="categoryArb" value="<?php echo $editRow['category_arabic'];?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
