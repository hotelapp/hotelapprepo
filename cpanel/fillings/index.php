<?php
require('../admin_header.php');

if($_SESSION['hotelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
   
//delete row in index page

function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>

    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Filling</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post">
                            <input type="text" name="searchName" id="" placeholder="Filling Name" value="<?php echo @$_REQUEST['searchName']; ?>">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                        </form>
                    </div>
                    <div class="page_nav_menu">
                        <ul class="page_nav">
                            <li>
                                <a href="add.php">
                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
<?php	
$cond="1";
if(@$_REQUEST['searchName'])
{
	$cond=$cond." and ".TABLE_FILLINGS.".filling_name like'%".$_POST['searchName']."%'";
}

?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Filling category</th>
                            <th>Filling Name</th>
                            <th>Filling Name Arabic</th>
                            <th>Price</th>                            
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=0;                   
                    $selectAll = "SELECT ".TABLE_FILLINGS.".ID,
                    					 ".TABLE_FILLING_CATEGORY.".category,
                    					 ".TABLE_FILLINGS.".filling_name,
                    					 ".TABLE_FILLINGS.".filling_name_arabic,
                    					 ".TABLE_FILLINGS.".price                    					
                    					 FROM ".TABLE_FILLINGS.",".TABLE_FILLING_CATEGORY."
                    					 WHERE ".TABLE_FILLING_CATEGORY.".ID=".TABLE_FILLINGS.".filling_category_id
                    					 AND  $cond ORDER BY ".TABLE_FILLINGS.".ID DESC";
                   // echo $selectAll;
                    $result = $db->query($selectAll);
                    if(mysql_num_rows($result)==0)
                    {
					?>
						<tr><td colspan="6" align="center">There is no data in list. </td></tr
					<?php
					}
					else
					{
	                    while ($row = mysql_fetch_array($result)) {
	                    	$tableId=$row['ID'];	                    	               
	                        ?>
	                     <tr>                            
	                        <td><?php echo ++$i; ?></td>
                            <td><?php echo $row['category']; ?></td>
	                        <td><?php echo $row['filling_name']; ?></td>
	                        <td><?php echo $row['filling_name_arabic']; ?></td>	
	                        <td><?php echo $row['price']; ?></td>			                            
	                        <td>	                        
				            <a class="show_table_lnk show_table_lnk_edit" href="edit.php?id=<?php echo $row['ID'];?>">Edit</a>
				           <a class="show_table_lnk show_table_lnk_del"  onclick="return delete_type();" href="do.php?id=<?php echo $row['ID']; ?>&op=delete">Delete</a>
	                        </td>
	                    </tr>
	                        <?php
	                    }
                    }
                    ?>
				                   
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>