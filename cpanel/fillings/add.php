<?php
require('../admin_header.php');

if($_SESSION['hotelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form" enctype="multipart/form-data">
                <div class="bd_panel_head">
                    <h3>FILLING REGISTRATION</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 form_block_row">
                        	<div class="form_block" >
                                <label>Category<span class="valid">*</span></label>
	                                <select name="filling_category_id" class="pass_to_table" required="">
                                    <option value="">Select</option>
                                    <?php
                                    $select = "select ID,category,category_arabic 
                                    			from " . TABLE_FILLING_CATEGORY . " 
			                                    Order by category";
                                    $res = $db->query($select);
                                    while ($row = mysql_fetch_array($res)) {
                                        ?>
                                        <option
                                            value="<?php echo $row['ID']; ?>"><?php echo $row['category']."(".$row['category_arabic'].")"; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form_block">
                                <label>Filling Name <span class="valid">*</span></label>
                                <input type="text" name="filling_name" id="filling_name" required="">
                            </div>
                            <div class="form_block">
                                <label>Filling Name - Arabic</label>
                                <input type="text" name="filling_name_arabic" id="filling_name_arabic">
                            </div>
                            <div class="form_block">
                                <label>Price <span class="valid">*</span></label>
                                <input type="text" name="price" id="price" required="">
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
