<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['hotelId']=="")
{
	header("location:../../logout.php");
}

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':

        if (!$_REQUEST['filling_name']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
			$filling_category_id	= $App->convert($_REQUEST['filling_category_id']);
            $filling_name			= $App->capitalize($_REQUEST['filling_name']); 
            
            $existId = $db->existValuesId(TABLE_FILLINGS, "filling_category_id='$filling_category_id' and filling_name=$filling_name");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            {
                $data['filling_category_id']	=	$filling_category_id;
                $data['filling_name']			=	$filling_name;
                $data['filling_name_arabic']	=	$App->convert($_REQUEST['filling_name_arabic']);
               	$data['price']					=	$App->convert($_REQUEST['price']);
                
                $success = $db->query_insert(TABLE_FILLINGS, $data);
			}

                $db->close();
				if ($success) 
                {
                    //$_SESSION['msg'] = " Details Added Successfully";
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
                }               
                else
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
                }
                 header("location:index.php");
           }
        break;
        
     case 'edit':
    	$editId = $_REQUEST['editId'];    	
    	if (!$_REQUEST['filling_name']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
			$filling_category_id	= $App->convert($_REQUEST['filling_category_id']);
            $filling_name			= $App->capitalize($_REQUEST['filling_name']); 
            
            $existId = $db->existValuesId(TABLE_FILLINGS, "filling_category_id='$filling_category_id' and filling_name=$filling_name");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            {
                $data['filling_category_id']	=	$filling_category_id;
                $data['filling_name']			=	$filling_name;
                $data['filling_name_arabic']	=	$App->convert($_REQUEST['filling_name_arabic']);
               	$data['price']					=	$App->convert($_REQUEST['price']);
                
                $success = $db->query_update(TABLE_FILLINGS, $data, "ID = $editId");
			}

                $db->close();
				if ($success) 
                {
                    //$_SESSION['msg'] = " Details Added Successfully";
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details updated successfully");                
                }               
                else
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to update details. Please try again.");                   
                }
                 header("location:index.php");
           }
        break;
    	
    // DELETE SECTION
    case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success = 0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
  
		$success= @mysql_query("DELETE FROM `".TABLE_FILLINGS."` WHERE ID='{$deleteId}' ");			
        $db->close();
        if ($success) {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } else {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
        }
        header("location:index.php");
        break;
}
?>