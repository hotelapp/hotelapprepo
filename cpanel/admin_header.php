<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['hotelId']=="")
{
	header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Ebzilon</title>
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="../../css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../../css/perfect-scrollbar.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.structure.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.theme.css">
	<link rel="stylesheet/less" type="text/css" href="../../css/timepicker.less">


	<link rel="stylesheet" type="text/css" href="../../css/style.css">

	<script src="../../js/jquery-2.2.1.min.js"></script>
	<script src="../../js/jquery-ui.min.js"></script>
	<script src="../../js/bootstrap.min.js"></script>
	<script src="../../js/perfect-scrollbar.jquery.js"></script>
	<script src="../../js/bootstrap-timepicker.js"></script>
	<script src="../../js/script.js"></script>

        <script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>

</head>
<body>
<div class="side_panel opened">
	<div class="app_logo">
		<img src="../../images/logo.png" alt="" /> <span>Ebzilon</span>
	</div>
	<div class="user_det">
            
	</div>
	<div class="side_nav_wrapper">
		<ul class="side_nav">
			<li>
				<a href="../meal_type">
					<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
					<span class="side_nav_text">Meal Type</span>
				</a>
			</li>
			<li>
				<a href="../menu_items">
					<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
					<span class="side_nav_text">Menu Items</span>
				</a>
			</li>
			<li class="has_child">
				<a href="#">
					<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
					<span class="side_nav_text">Fillings</span>
					<span class="side_nav_drop_ico"></span>
				</a>
				<ul class="side_sub_nav">
					<li>
						<a href="../filling_category/index.php">
							<span class="side_nav_ico"><i class="ion ion-ios-browsers"></i></span>
							<span class="side_nav_text">Filling Category</span>
						</a>
					</li>
					<li>
						<a href="../fillings/index.php">
							<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
							<span class="side_nav_text">Fillings</span>
						</a>
					</li>
					<li>
						<a href="../filling_type/index.php">
							<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
							<span class="side_nav_text">Filling Type</span>
						</a>
					</li>
					
				</ul>
			</li>
		</ul>
	</div>
</div>
<div class="wrapper give_way">
<div class="header">
	<div class="container-fluid">
		<div class="nav_toggler">
			<span class="ion ion-android-menu"></span>
		</div>
		<div class="header_nav">
			<!--<ul class="header_main_nav">
				<li><a href="../account_customer">Customer</a></li>
				<li><a href="../account_supplier">Supplier</a></li>
			</ul>-->
		</div>
		<div class="header_user_controls">
			<ul class="header_main_nav">
				<li class="has_child">
					<a href="#">
						
                        <span class="nav_text"><?php echo "Admin";?></span>
                        
                        <span class="drop_ico"><i class="ion ion-ios-arrow-down"></i></span>
                        <div class="bd_clear"></div>
                    </a>
					<ul class="header_sub_nav">
						<li><a href="../changePassword">Change Password</a></li>
						<li><a href="../../logout.php">Log Out</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="bd_clear"></div>
	</div>
</div>
<div class="container-fluid">