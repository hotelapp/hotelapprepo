<?php
require('../admin_header.php');

if($_SESSION['hotelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
$editId		=	$_REQUEST['id'];
$editSelect = 	"select  * from ". TABLE_MEAL_TYPES." where id='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=edit" class="default_form" enctype="multipart/form-data">
            <input type="hidden" name="editId" value="<?php echo $editId; ?>">
                <div class="bd_panel_head">
                    <h3>Meal Types</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 form_block_row">
                        	<div class="form_block">
                                <label>Meal Type- English<span class="valid">*</span></label>
                                <input type="text" name="meal_type" required="" id="meal_type" value="<?php echo $editRow['meal_type'];?>">
                            </div>
                            <div class="form_block">
                                <label>Meal Type- Arabic</label>
                                <input type="text" name="meal_type_arabic" id="meal_type_arabic"  value="<?php echo $editRow['meal_type_arabic'];?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
