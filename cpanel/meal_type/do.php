<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['hotelId']=="")
{
	header("location:../../logout.php");
}

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':

        if (!$_REQUEST['meal_type']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
			
            $meal_type			= $App->convert($_REQUEST['meal_type']);                      
            $meal_type_arabic			= $App->convert($_REQUEST['meal_type_arabic']);                      
			

            $existId = $db->existValuesId(TABLE_MEAL_TYPES, "meal_type='$meal_type'");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            {
                $data['meal_type']		=	$meal_type;
                $data['meal_type_arabic']		=	$meal_type_arabic;
                $data['created_at']		= "NOW()";
                $data['updated_at']		= "NOW()";
               
                $success = $db->query_insert(TABLE_MEAL_TYPES, $data);
                $db->close();

                if ($success) 
                {
                    //$_SESSION['msg'] = " Details Added Successfully";
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
                }
                 header("location:index.php");
            }
        }
        break;
        
        
         case 'edit':
    	$editId = $_REQUEST['editId'];
    	if (!$_REQUEST['meal_type']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            
            $meal_type		= $App->convert($_REQUEST['meal_type']);
            $meal_type_arabic		= $App->convert($_REQUEST['meal_type_arabic']);
             
              $existId = $db->existValuesId(TABLE_MEAL_TYPES, "meal_type='$meal_type' and id!='$editId'");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            { 
            $data['meal_type']	=	$meal_type;
            $data['meal_type_arabic']	=	$meal_type_arabic;
           	$data['created_at']		= "NOW()";
           	$data['updated_at']		= "NOW()";                   
           
					$success = $db->query_update(TABLE_MEAL_TYPES, $data, "id = $editId");
				
                $db->close();
				if ($success) 
                {
                    //$_SESSION['msg'] = " Details Added Successfully";
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details uploaded successfully");                
                } 
              
                else
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to upload details. Please try again.");                   
                }
                 header("location:index.php");
            }
        }
        break;
    
    // DELETE SECTION
    case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success = 0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
  
			$success1= @mysql_query("DELETE FROM `".TABLE_MEAL_TYPES."` WHERE id='{$deleteId}'");
			
        $db->close();
        if ($success1) {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } else {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
        }
        header("location:index.php");
        break;
        
        
        
        
}
?>