<?php
require('../admin_header.php');

if($_SESSION['hotelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
   
//delete row in index page

function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>

    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Menu Items</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post">
                            <input type="text" name="menu_item" id="" placeholder="Menu Item" value="<?php echo @$_REQUEST['menu_item']; ?>">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                        </form>
                    </div>
                    <div class="page_nav_menu">
                        <ul class="page_nav">
                            <li>
                                <a href="add.php">
                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
<?php	
$cond="1";
if(@$_REQUEST['menu_item'])
{
	$cond=$cond." and ".TABLE_MENU_ITEMS.".menu_item like'%".$_POST['menu_item']."%'";
}

?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Meal Type</th>
                            <th>Menu Item - English</th>
                            <th>Menu Item - Arabic</th>
                            <th>Image</th>                            
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=0;
                    
                    $selectAll= "SELECT ".TABLE_MENU_ITEMS.".id,
                    					".TABLE_MENU_ITEMS.".menu_item,
                    					".TABLE_MENU_ITEMS.".menu_item_arabic,
                    					".TABLE_MENU_ITEMS.".image,
                    					".TABLE_MEAL_TYPES.".meal_type,
                    					".TABLE_MEAL_TYPES.".meal_type_arabic
                    			FROM ".TABLE_MEAL_TYPES.",".TABLE_MENU_ITEMS."
                    			WHERE ".TABLE_MENU_ITEMS.".meal_type_id = ".TABLE_MEAL_TYPES.".id
                    			AND $cond ORDER BY id desc";
                    
                    //echo $selectAll;die;
                    $result = $db->query($selectAll);
                    if(mysql_num_rows($result)==0)
                    {
					?>
						<tr><td colspan="6" align="center">There is no data in list. </td></tr
					<?php
					}
					else
					{
	                    while ($row = mysql_fetch_array($result)) {
	                    	
	                    	$mealType = $row['meal_type']."(".$row['meal_type_arabic'].")";
	                    	
	                        ?>
	                     <tr>                            
	                        <td><?php echo ++$i; ?></td>
                            <td><?php echo $mealType; ?></td>
	                        <td><?php echo $row['menu_item']; ?></td>
	                        <td><?php echo $row['menu_item_arabic']; ?></td>
	                        <td><img src="<?php if($row['image']){ echo "../../public/uploads/".$row['image'];}else { echo "../../uploads/menuitems/1478769366.jpg";} ?>" width="50" height="50"></td>				                            
	                        <td>
	                        
	                       
	                        	<a class="show_table_lnk show_table_lnk_edit" href="edit.php?id=<?php echo $row['id'];?>">Edit</a>
	                        	<a class="show_table_lnk show_table_lnk_del" onclick="return delete_type();" href="do.php?id=<?php echo $row['id'];?>&op=delete">Delete</a>
	                        	
	                        	<a class="show_table_lnk show_table_lnk_view" href="view.php?id=<?php echo $row['id'];?>">View</a>
	                       
	                       
	                        
	                        </td>
	                    </tr>
	                        <?php
	                    }
                    }
                    ?>
				                   
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>