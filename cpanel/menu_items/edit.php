<?php
require('../admin_header.php');

if($_SESSION['hotelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
$editId		=	$_REQUEST['id'];
$editSelect = 	"select  * from ". TABLE_MENU_ITEMS." where id='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=edit" class="default_form" enctype="multipart/form-data">
            <input type="hidden" name="editId" value="<?php echo $editId; ?>">
                <div class="bd_panel_head">
                    <h3>Edit Menu Items</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Meal Types <span class="valid">*</span></label>
                                    <select name="meal_type" class="pass_to_table" required="">
	                                    <option value="">Select</option>
	                                    <?php 
	                                    $relQry	=	mysql_query("SELECT * FROM ".TABLE_MEAL_TYPES."");
	                                    while($relRow	=	mysql_fetch_array($relQry))
	                                    {
	                                    ?>
										<option value="<?php echo $relRow['id']; ?>" <?php if($editRow['meal_type_id']==$relRow['id']){ echo "selected";}?> ><?php echo $relRow['meal_type']."(".$relRow['meal_type_arabic'].")";?></option>
										<?php }?>
									</select>
								</div>
						</div>
						
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">     
							<div class="form_block">
								<label>Menu Item - English<span class="valid">*</span></label>
								<input type="text" name="menu_item" id="menu_item"  required="" value="<?php echo $editRow['menu_item']; ?>">
							</div>
							
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">     
							<div class="form_block">
								<label>Menu Item - Arabic</label>
								<input type="text" name="menu_item_arabic" id="menu_item_arabic" value="<?php echo $editRow['menu_item_arabic']; ?>">
							</div>
							
						</div>
						
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Calories</label>
								<input type="text" name="calories" id="calories" value="<?php echo $editRow['calories']; ?>">
							</div>
						</div>
						
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Fat</label>
								<input type="text" name="fat" id="fat" value="<?php echo $editRow['fat']; ?>">
							</div>
						</div>
					</div>
					<div class="row">
						
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Price</label>
								<input type="text" name="price" id="price" value="<?php echo $editRow['price']; ?>">
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Description</label>
								<textarea name="description"><?php echo $editRow['description']; ?> </textarea>                           
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Description - Arabic</label>
								<textarea name="description_arabic"> <?php echo $editRow['description_arabic']; ?></textarea>
							</div>
						</div>
					</div>
					<div class="row"> 
						<div class="col-lg-2 col-sm-2 col-md-2">
							<div class="form_block">
								<label>Upload Image</label>
								<input type="file" name="image" />
							</div>
						</div>
					</div>   
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
