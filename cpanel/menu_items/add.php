<?php
require('../admin_header.php');
if(@isset($_SESSION['msg'])){
	echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>

<div class="row">
	<div class="col-lg-12">
		<div class="bd_panel bd_panel_default bd_panel_shadow">
			<form method="post" action="do.php?op=index" class="default_form" onsubmit="return validateForm()" enctype="multipart/form-data">
				<div class="bd_panel_head">
					<h3>Add Menu Items</h3>
				</div>
				<div class="bd_panel_body">
					<div class="row">
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Meal Types <span class="valid">*</span></label>
                                    <select name="meal_type" class="pass_to_table" required="">
	                                    <option value="">Select</option>
	                                    <?php 
	                                    $relQry	=	mysql_query("SELECT * FROM ".TABLE_MEAL_TYPES."");
	                                    while($relRow	=	mysql_fetch_array($relQry))
	                                    {
	                                    ?>
										<option value="<?php echo $relRow['id']; ?>"><?php echo $relRow['meal_type']."(".$relRow['meal_type_arabic'].")";?></option>
										<?php }?>
									</select>
								</div>
						</div>
						
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">     
							<div class="form_block">
								<label>Menu Item - English<span class="valid">*</span></label>
								<input type="text" name="menu_item" id="menu_item"  required="">
							</div>
							
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">     
							<div class="form_block">
								<label>Menu Item - Arabic</label>
								<input type="text" name="menu_item_arabic" id="menu_item_arabic">
							</div>
							
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Calories</label>
								<input type="text" name="calories" id="calories">
							</div>
						</div>
						
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Fat</label>
								<input type="text" name="fat" id="fat">
							</div>
						</div>
					</div>
					<div class="row">
						
						
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Price</label>
								<input type="text" name="price" id="price">
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Description</label>
								<textarea name="description"> </textarea>                           
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Description - Arabic</label>
								<textarea name="description_arabic"> </textarea>
							</div>
						</div>
					</div>
					<div class="row"> 
						<div class="col-lg-2 col-sm-2 col-md-2">
							<div class="form_block">
								<label>Upload Image</label>
								<input type="file" name="image" />
							</div>
						</div>
					</div>   
					<div name="validDiv" id="validDiv" style="color:#FF6600;"></div>               
					<div class="row">
						<div class="col-lg-12">
							<div class="airline_submit">                             
								<input type="submit" value="SAVE">
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>                
	</div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
