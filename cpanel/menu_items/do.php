<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['hotelId']=="")
{
	header("location:../../logout.php");
}

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':

        if (!$_REQUEST['menu_item']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
            
			$menu_item			= $App->convert($_REQUEST['menu_item']); 
            
            $existId = $db->existValuesId(TABLE_MENU_ITEMS, "menu_item='$menu_item'");
            
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            {
                $data['menu_item']			=	$App->convert($_REQUEST['menu_item']);
                $data['menu_item_arabic']	=	$App->convert($_REQUEST['menu_item_arabic']);
                $data['meal_type_id']		=	$App->convert($_REQUEST['meal_type']);
                $data['description']		=	$App->convert($_REQUEST['description']); 
                $data['description_arabic']	=	$App->convert($_REQUEST['description_arabic']); 
                $data['calories']			=	$App->convert($_REQUEST['calories']);
                $data['fat']				=	$App->convert($_REQUEST['fat']);
                $data['price']				=	$App->convert($_REQUEST['price']);
                $data['created_at']		= "NOW()";
           		$data['updated_at']		= "NOW()";
                
                if($_FILES["image"]["name"])
                {
                	$image	=	$App->imageValidation($_FILES["image"]["name"]);
	                if($image==1)
	                {
						$date	= 	date("YmdHis");
		                $temp 	= 	explode(".", $_FILES["image"]["name"]);
						$newfilename = $date.'.' . end($temp);
						$check = move_uploaded_file($_FILES["image"]["tmp_name"], "../../public/uploads/menuitems/" . $newfilename);
						if($check)
		                {
							$img="/menuitems/" . $newfilename;		
						}

						$data['image']			=	$App->convert($img);
						$success = $db->query_insert(TABLE_MENU_ITEMS, $data);
					}
				}
				else
				{
					$success = $db->query_insert(TABLE_MENU_ITEMS, $data);
				}

                $db->close();
				if ($success) 
                {
                    //$_SESSION['msg'] = " Details Added Successfully";
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
                } 
                 else if ($image==0 and $success==0)
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Invalid image format.");                   
                }
                else
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
                }
                 header("location:index.php");
            }
        }
        break;
        
     case 'edit':
    	$editId = $_REQUEST['editId'];
    	if (!$_REQUEST['menu_item']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = $image = 0;
            $menu_item			= $App->convert($_REQUEST['menu_item']);
            
              $existId = $db->existValuesId(TABLE_MENU_ITEMS, "menu_item='$menu_item' and id!='$editId'");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            { 
             $data['menu_item']			=	$App->convert($_REQUEST['menu_item']);
                $data['menu_item_arabic']	=	$App->convert($_REQUEST['menu_item_arabic']);
                $data['meal_type_id']		=	$App->convert($_REQUEST['meal_type']);
                $data['description']		=	$App->convert($_REQUEST['description']); 
                $data['description_arabic']	=	$App->convert($_REQUEST['description_arabic']); 
                $data['calories']			=	$App->convert($_REQUEST['calories']);
                $data['fat']				=	$App->convert($_REQUEST['fat']);
                $data['price']				=	$App->convert($_REQUEST['price']);
                $data['created_at']		= "NOW()";
           		$data['updated_at']		= "NOW()";                 
            if ($_FILES["image"]["name"]) {
				$res=mysql_query("select image from ".TABLE_MENU_ITEMS." where id='{$editId}'");
				$row=mysql_fetch_array($res);
				$imageDel=$row['image'];
				if (file_exists($imageDel))
				{
					unlink($imageDel);
				}
			}
           if($_FILES["image"]["name"])
                {
                	$image	=	$App->imageValidation($_FILES["image"]["name"]);
	                if($image==1)
	                {
						$date	= 	date("YmdHis");
		                $temp 	= 	explode(".", $_FILES["image"]["name"]);
						$newfilename = $date.'.' . end($temp);
						if(move_uploaded_file($_FILES["image"]["tmp_name"], "../../public/uploads/menuitems/" . $newfilename))
		                {
							$img="/menuitems/" . $newfilename;			
						}

						$data['image']			=	$App->convert($img);
						$success = $db->query_update(TABLE_MENU_ITEMS, $data, "id = $editId");
					}
				}
				else
				{
					$success = $db->query_update(TABLE_MENU_ITEMS, $data, "id = $editId");
				}

                $db->close();
				if ($success) 
                {
                    //$_SESSION['msg'] = " Details Added Successfully";
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details uploaded successfully");                
                } 
                
                else if ($image==0 and $success==0)
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Invalid image format.");                   
                }
                else
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to upload details. Please try again.");                   
                }
                 header("location:index.php");
            }
        }
        break;
    
    // DELETE SECTION
    case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success = 0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
  
            $res=mysql_query("select image from ".TABLE_MENU_ITEMS." where id='{$deleteId}'");
			$row=mysql_fetch_array($res);
			$image=$row['image'];	
			$success1= @mysql_query("DELETE FROM `".TABLE_MENU_ITEMS."` WHERE id='{$deleteId}'");
			if($success1)					
			{			//echo $image;die;																														
				if (file_exists($image)) 	
				{
				unlink($image);					
				} 						
			}	
        
        $db->close();
        if ($success1) {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } else {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
        }
        header("location:index.php");
        break;
}
?>